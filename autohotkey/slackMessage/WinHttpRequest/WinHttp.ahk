﻿;Prepare our WinHttpRequest object
HttpObj := ComObjCreate("WinHttp.WinHttpRequest.5.1")
;HttpObj.SetProxy(2,"localhost:8888") ;Send data through Fiddler
HttpObj.SetTimeouts(6000,6000,6000,6000) ;Set timeouts to 6 seconds
;HttpObj.Option(6) := False ;disable location-header rediects

;Set our URLs
reqURL := "https://hooks.slack.com/services/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"

reqData := "payload={'text': 'This is a line of text in a channel'}"

MsgBox, %reqData%
;Step 4/5
HttpObj.Open("POST",reqURL)
HttpObj.SetRequestHeader("Content-Type","application/x-www-form-urlencoded")
HttpObj.Send(reqData)
req := HttpObj.ResponseText

MsgBox, %req%
