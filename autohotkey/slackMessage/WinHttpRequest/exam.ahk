﻿#Include %A_LineFile%\..\WinHttpRequest.ahk
#Include %A_LineFile%\..\JSON.ahk

json_str =
(
{"text": "This is posted to #general and comes from *monkey-bot*."}	
)

parsed := JSON.Load(json_str)

parsed_out := Format("
(Join`r`n
String: {}
Number: {}
Float:  {}
true:   {}
false:  {}
null:   {}
array:  [{}, {}, {}]
object: {{}A:""{}"", H:""{}"", K:""{}""{}}
)"
, parsed.str, parsed.num, parsed.float, parsed.true, parsed.false, parsed.null
, parsed.array[1], parsed.array[2], parsed.array[3]
, parsed.object.A, parsed.object.H, parsed.object.K)

stringified := JSON.Dump(parsed,, 4)
stringified := StrReplace(stringified, "`n", "`r`n") ; for display purposes only

msg := "payload={'text':'This is a line of text'}"

r := WinHttpRequest("https://hooks.slack.com/services/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", InOutData := "payload=%json_str%", InOutHeaders := Headers(), "Method: POST")
MsgBox, % (r = -1) ? "successful" : (r = 0) ? "Timeout" : "No response"
MsgBox, % InOutData
MsgBox, % InOutHeaders
Return

Headers(referer = "")
{
	Headers =
	( LTrim
		Referer: %referer%
		User-Agent: Opera/9.80 (Windows NT 5.1) Presto/2.12.388 Version/12.16
	)
	
	Return Headers
}