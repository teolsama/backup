﻿;Prepare our WinHttpRequest object
HttpObj := ComObjCreate("WinHttp.WinHttpRequest.5.1")
;HttpObj.SetProxy(2,"localhost:8888") ;Send data through Fiddler
HttpObj.SetTimeouts(6000,6000,6000,6000) ;Set timeouts to 6 seconds
;HttpObj.Option(6) := False ;disable location-header rediects

;Set our URLs
loginSiteURL := "http://www.autohotkey.com/board/index.php?app=core&module=global&section=login"
loginURL := "https://hooks.slack.com/services/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"

loginBody := "payload={'text': 'This is a line of text in a channel'}"

MsgBox, %loginBody%
;Step 4/5
HttpObj.Open("POST",loginURL)
HttpObj.SetRequestHeader("Content-Type","application/x-www-form-urlencoded")
HttpObj.Send(loginBody)
req := HttpObj.ResponseText

MsgBox, %req%
