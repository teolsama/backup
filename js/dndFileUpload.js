/*
 * 파일 업로드 플러그인 ver 0.2
 * 
 * 옵션 설명 
 * globalProperties: global.properties 에 세팅된 파일업로드 관련 정보 Globals.FileUpload.Promotion 이라면 Promotion 으로 값을 넘김.
 * asyncUpload: false, 비동기 여부 true 면 비동기로 작동함.
 * notUploadAlertMsg: true, 업로드시 업로드할 파일이 없을때 경고창 띄울지 여부.
 * formName: 비동기로 업로드시 생성할 form 이름
 * fileType : 허용할 파일타입 jpg,png 등...
 * fileSize : 파일용량 Mega 기준
 * imgWidth : 미리보기 너비값
 * imgHeight : 미리보기 높이값
 * inputFileName : 파일 업로드할 input name값
 * inputRtnFileName : 파일 업로드후 받을 파일명을 받을  input name값.
 * defaultImgUrl : 로딩시 기본으로 표시할 이미지 url
 * returnPath :  Y 이면 inputRtnFileName 에 전체 url 값을 가져옴. N 이면 파일명만 가져옴.
 * closeIconUse: Y or N 이미지 클로즈버튼 사용여부 -> 기능은 이미지 삭제기능 (화면에서만 삭제함, 서버삭제 안함)
 * closeIconUrl: "/plugin/jqGrid/css/images/ui-icons_888888_256x240.png" 
 * closeIconX: 클로즈 아이콘 위치  x좌표(한 파일에 여러 아이콘 들어있는 파일에서 가져올때)
 * closeIconY: 클로즈 아이콘 위치 y좌표(한 파일에 여러 아이콘 들어있는 파일에서 가져올때)
 * closeIconWidth: 아이콘 크기
 * closeIconHeight: 아이콘 크기
 * prevFileName : 업로드할 파일명 앞에 붙일 이름
 * nextFileName : 업로드할 파일명 뒤에 붙일 이름
 * 
 * 사용 방법 * 
<div id="jqFileUpload2"></div> 일때,

$("#jqFileUpload1").dndFileUpload({
	fileType: "jpg", 
	fileSize: "10",  
	imgWidth: "200", 
	imgHeight: "40", 
	inputFileName: "reportFile1",
	inputRtnFileName: "reportFileName1",
	defaultImgUrl : "/image/lvtmgmt/logo.png"
	returnPath : "Y"
	closeIconUse: "Y", 
	closeIconUrl: "/plugin/jqGrid/css/images/ui-icons_888888_256x240.png",
	closeIconX: "-97px",
	closeIconY: "112px",
	closeIconWidth: "15",
	closeIconHeight: "15",
});

* 미리보기에 한글명으로 된 이미지 파일이 안보일때 , get 방식으로 파일명을 넘겨서 미리보여줄때는 톰캣 server.xml 아래 설정을 확인 URIEncoding="UTF-8"
* <Connector connectionTimeout="20000" port="8080" protocol="HTTP/1.1" redirectPort="8443"  URIEncoding="UTF-8" />

 * */

(function ($){
	$.fn.dndFileUpload = function (options){

		var opts = $.extend({}, $.fn.dndFileUpload.defaults, options );
		
		var dndFileElement = $(this);
		
		// Upload 대상 파일 설정 여부 flag 초기화
		dndFileElement.data("isSelected", false);
		
		var divObj, inputObj, inputRtnObj, closeIconObj, formObj;
		
		var chkPreUpload = false;

		var extRegExp = new RegExp(opts.fileType, 'i');
		
		var chkHtml5 = true;
	    if (window.File && window.FileReader && window.FileList && window.Blob){
	    	chkHtml5 = true;
	    	//informHtml5 = "드래그&드랍 <br /> 클릭"; 
	    	informHtml5 = "드래그&드랍"; /* 클릭도 가능하지만 표시는 안함 */
	    }else{
	    	chkHtml5 = false;
	    	informHtml5 = "마우스오버";
	    }
	    
		var parentObjWidth = $(this).parent().width();
		var parentObjHeight = $(this).parent().height(); //- 35; //input 개체만큼 미리 공간확보용 35px 설정

		var objWidth = opts.imgWidth;
		var objHeight = opts.imgHeight;
		
		//비율 비교
		if (opts.imgHeight/opts.imgWidth > parentObjHeight/parentObjWidth){
			// 설정된 이미지 높이보다 부모개체가 작으면, 높이는 부모개체만큼만 , 너비는 부모개체 너비에서 이미지지비율을 곱한다.
			if (parentObjHeight > opts.imgHeight){
				objHeight = opts.imgHeight;
				objWidth = opts.imgWidth;	
			}else{
				objHeight = parentObjHeight;
				objWidth = parentObjHeight * opts.imgWidth / opts.imgHeight;
			}
		}else{
			// parentObjWidth 기준. 즉, 가로기준.
			if (parentObjWidth > opts.imgWidth){
				objWidth = opts.imgWidth;	
				objHeight = opts.imgHeight;
			}else{
				objWidth = parentObjWidth;
				objHeight = parentObjWidth * opts.imgHeight / opts.imgWidth;
			}					
		}

		var onsubmitEventCnt = 0; // 서브밋이벤트 생성 카운트
		var submitCnt = 0; // 서브밋실행 카운트, 파일을 바꿀경우 카운트가 일치할 경우만 업로드 실행(formOnsubmit)
		
		// 크롬에서는 이벤트 발생 시점에서 파일 데이타를 가져가므로 targetData.files 를 가져가지 못한다.
		// 그래서 파일데이타를 전역으로 가지고 있다가 이벤트 발생 시점에 가져간다.
		var _uploadFileData;
		var formOnsubmit;
		var imagePreview = function(targetData, thisObj){

	        // html5 지원브라우저면
	        if (chkHtml5){
				var reader = new FileReader();
				reader.onload = function (event){
					//$("#imgSrc").attr("src", event.target.result);	
					$(thisObj).css({
						"background-image" : "url('"+ event.target.result +"')",
	                    "background-size" : "contain",
	                    "background-repeat" : "no-repeat",
					}); 
				};
				reader.readAsDataURL(targetData.files[0]);
				
				onsubmitEventCnt += 1;
				$(inputRtnObj).val(opts.defaultImgUrl);
				
				_uploadFileData = targetData.files;
				
				if(opts.asyncUpload){
					$(formObj).off("submit");
					$(formObj).on("submit",function(e){
						if(opts.asyncUpload){	//동적업로드시엔 submit 차단
							e.preventDefault();
						}

						// 크롬에서 안되네;;; 
						//formOnsubmit(targetData.files, onsubmitEventCnt);
						// 일단 이럴게 해본다. 
						//formOnsubmit(targetData.files[0], onsubmitEventCnt);
						// 안되네;;
						formOnsubmit(_uploadFileData, submitCnt+1);
					});
					
				}else{
					formOnsubmit(_uploadFileData, onsubmitEventCnt);
				}
				
	        }else{
	        	if ($(targetData).val().indexOf("fakepath") > -1){
	        		alert("고객님께서 접속하신 브라우저는 HTML5가 지원되지 않는 브라우저입니다.(HTML5 지원: 인터넷익스플로러10이상,크롬,파이어폭스 )\
	        				\n'인터넷 옵션 > 보안 > 신뢰할 수 있는 사이트' 에 현재사이트를 등록해주셔야 미리보기가 가능합니다.\
	        				\n(https 는 체크해제 후 등록해야합니다.)\n\n미리보기가 되지 않아도 업로드는 가능합니다.");
	        	}else{
		            var newPreview = thisObj;
		            newPreview[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").Enabled = true;
		            newPreview[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "scale";		            
		            newPreview[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(targetData).val();
	        	}
	        	
	        	//html5 미 지원 브라우저는 submit 발생시 파일 업로드시 ajax 동기화옵션(async = false)이 작동하지 않음으로 미리보기 동작시에 바로 업로드 처리함.
	        	$(formObj).submit();
	        	chkPreUpload = true;
	        }	
	        
	        // 실제 업로드 -> ie9 이하 ajax 업로드 문제로 적용안함
	        //handleFileUpload(targetData.files);
	        
			// Upload 대상 파일 설정 여부 flag 설정
	        dndFileElement.data("isSelected", true);
		};
		
		// 서브밋 발생시 처리할 부분
		formOnsubmit = function(data, cntChk){
			submitCnt += 1;
			var hostName = location.protocol + "//" + location.host;
			// 여러번 파일 업로드 시킬때 마지막 부분만 업로드 동작, 그전 파일과 바뀌었을때만 업로드 처리.
			if (cntChk == submitCnt && $(divObj).css("background-image").slice(4,-1).replace(/"/g,"").replace(hostName,"") != opts.defaultImgUrl.replace(hostName,""))
				handleFileUpload(data);
		};
		
		// 파일 업로드 처리
		var handleFileUpload = function(files)
	    {
            var fd = new FormData();
            fd.append('file', files[0]);

            fd.append('properties',opts.globalProperties);
            fd.append('prevfilename',opts.prevFileName);
            fd.append('nextfilename',opts.nextFileName);
            
            sendFileToServer(fd);
	    };

	    var getParam = "properties="+opts.globalProperties;
	    getParam += "&prevfilename="+opts.prevFileName;
	    getParam += "&nextfilename="+opts.nextFileName;
	    
	    var sendFileToServer = function(formData)
	    {
	        var uploadURL = "/lvtmgmt/common/fileUploadOreillyAjax.lv?"+ getParam; //Upload URL
	        $.ajax({
	            url: uploadURL,
	            type: "POST",
	            dataType: "json",
	            contentType:false,
	            processData: false,
	            cache: false,
	            async: false,
	            data: formData,
	            success: function(data){
	            	if (typeof data == 'object'){
	            		rtnResultProc(data);
	            	}           
	            },
	            error: function(data){
            		console.log("ajax loading error!!");
            		alert("ajax loading error!!\n접속상태를 확인해주세요");
	            }
	        }); 
	      
	    };
	    
	    // ajax 업로드후 처리 부분
		var rtnResultProc = function(data){
			//$(inputRtnObj).val(
			//					$(inputRtnObj).val() + ($(inputRtnObj).val() != "")? ","+ data.saveFileName : data.saveFileName
			//				   );
    		onsubmitEventCnt = 0;
    		submitCnt = 0;
			if (opts.asyncUpload){
	    		$(formObj).off("submit");
	    		$(formObj).on("submit",function(e){
	    			if(opts.notUploadAlertMsg)
	    				alert("저장할 파일이 없습니다.");
					return false;
				});				
			}
			if (opts.returnPath == "Y"){
				$(inputRtnObj).val(data.saveFileUrl);	
			}else{
				$(inputRtnObj).val(data.saveFileName);
			}

			if (opts.rtnFunc != null)
				opts.rtnFunc();
		};
		
		// 이미지 보여주는 부분
		divObj = $("<div>",{
			addClass: "dragAndDropDiv_"+ parseInt($.now() * Math.random()).toString() ,
			css: {
				"filter": "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src='"+ opts.defaultImgUrl +"')",/* ie9 이하에서 이미지 미리보기위한 css, 미리 주지 않으면 안나오는데; 이유는 모르겠음. */
                "border": "2px dashed #92AAB0",
                "width": objWidth +"px",
                "height": objHeight +"px",
                "color": "#92AAB0",
                "text-align": "center",
                "vertical-align": "middle",
                "margin": "0px",
                "padding": "0px",
                "font-size": "1em",
                "display": "table-cell",
                "text-overflow": "ellipsis",
                "white-space": "nowrap",
                "word-wrap": "normal"
			},
			html: informHtml5
		});
		
		//클릭시
		$(divObj).on("click",function(){
			//$("#myfile123").click();
			// trigger 로 변경
			$("#myfile123").trigger('click');
		});
		
		// 드래그 진입시  
	    $(divObj).on("dragenter",function(e){
	        e.stopPropagation();
	        e.preventDefault();
	        //$(this).effect("highlight");
	        $(this).css('border', '2px solid #0B85A1');
	    });
		
		// 드래그 위에 있을때   
	    $(divObj).on("dragover",function(e){
	        e.stopPropagation();
	        e.preventDefault();
	        //$(this).effect("highlight");
	    });
		
		// 드래그 나갈때   
	    $(divObj).on("dragleave",function(e){
	        e.stopPropagation();
	        e.preventDefault();
	        //$(this).effect("shake");
	    });
		
		// 드랍후 
	    $(divObj).on("drop",function(e){
	        $(this).css('border', '2px dotted #0B85A1');
	        e.preventDefault();
	        
	        var dataFile = e.originalEvent.dataTransfer;
	        
	        //alert(dataFile.files[0].type);
	        // type 으로 체크해야 정확하지만 html5 미지원브라우저와 호환을 위해서 확장자로만 체크함.
	        
	        var fileExt = dataFile.files[0].name.substring(dataFile.files[0].name.lastIndexOf("."));
			if(!fileExt.match(extRegExp)){
				window.focus();
				alert("업로드 가능한 파일종류가 아닙니다.\n"+ opts.fileType.replace("|",",") +"만 업로드 가능합니다.");
				return;
			}
	        
	        imagePreview(dataFile, this);
	        
	    });

		// close 아이콘
		if (opts.closeIconUse == "Y"){
			closeIconObj = $("<div>",{
				css: {
					"position": "absolute",
					"display": "none",
					"width" : opts.closeIconWidth,
					"height" : opts.closeIconHeight,
					"background": "url("+ opts.closeIconUrl +") "+ opts.closeIconX +" "+ opts.closeIconY +" ",
				}
			});	
		}
		
		// 마우스 오버시
	    $(divObj).on("mouseover",function(e){

			//var divOffset = $(this).offset();
			var divPosition = $(this).position();
	        $(closeIconObj).css({
				"top": divPosition.top,
				"left": $(this).width() + divPosition.left,
	        }).show();
			
	        $(inputObj).css({
				"top": divPosition.top + $(divObj).height(),
				"left": divPosition.left,
	        }).show();
	    });
		
		// 마우스 아웃시
	    $(divObj).on("mouseleave",function(e){
	    	//if()
	        $(inputObj).fadeOut();
	        $(closeIconObj).fadeOut();
	    });

		// CLOSE 아이콘  마우스 오버시
	    $(closeIconObj).on("mouseover",function(e){
	        $(this).stop(true).show();
	    });

		// CLOSE 아이콘  마우스 아웃시
	    $(closeIconObj).on("mouseleave",function(e){
	        $(this).stop(true).hide();
	    });

		// CLOSE 아이콘 마우스 클릭시 초기화
	    $(closeIconObj).on("click",function(e){
	    	inputObj.replaceWith(inputObj = inputObj.clone(true));
	    	//이미지 초기화 
	    	if(chkHtml5){
				$(divObj).css({
					"background-image" : "url('"+ opts.defaultImgUrl +"')",
	                "background-size" : "contain",
	                "background-repeat" : "no-repeat",
				}); 
				if (opts.asyncUpload){
					$(formObj).off("submit");
					$(formObj).on("submit",function(e){
						if(opts.notUploadAlertMsg)
							alert("저장할 파일이 없습니다.");
						return false;
					});
				}
	    	}else{
	    		// filter 는 jquery 로 변경이 안되는거 같음. 그냥 javascript로 처리.
	            var newPreview = $(divObj);
	            if (opts.defaultImgUrl == ""){
	            	newPreview[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").Enabled = false;
	            }else{
	            	newPreview[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = opts.defaultImgUrl;
	            }
	    	}

			// Upload 대상 파일 설정 여부 flag 설정
	    	dndFileElement.data("isSelected", false);
	    });
	    
		// 업로드할 파일
		inputObj = $("<input>",{
			id: opts.inputFileName,
			name: opts.inputFileName,
			type: "file",
			css: {
				"width" : $(divObj).css("width"),
				"display" : "none",
				"position": "absolute",
				"top": $(divObj).css("top")-15
			}
		});
		
		// ajax 업로드후 받을 파일명
		inputRtnObj = $("<input>",{
			id: opts.inputRtnFileName,
			name: opts.inputRtnFileName,
			type: "text",
			css: {
				"display" : "none",
			},
			value: opts.defaultImgUrl, //기본으로 defaultImagUrl 을 가지고 있음.
		
		});

		// ajax 업로드시 업로드할 global.properties 명
		inputPropertiesObj = $("<input>",{
			id: "properties",
			name: "properties",
			type: "text",
			value: opts.globalProperties,
			css: {
				"display" : "none",
			}
		});
		
		// ajax 업로드시 prevFileName 값
		inputPrevFileNameObj = $("<input>",{
			id: "prevfilename",
			name: "prevfilename",
			type: "text",
			value: opts.prevFileName,
			css: {
				"display" : "none",
			}
		});
		 
		// ajax 업로드시 nextFileName 값 
		inputNextFileNameObj = $("<input>",{
			id: "nextfilename",
			name: "nextfilename",
			type: "text",
			value: opts.nextFileName,
			css: {
				"display" : "none",
			}
		});
		
		// 마우스 오버시
	    $(inputObj).on("mouseover",function(e){
	    	$(this).stop(true).show();
	    });

		// 마우스 아웃시
	    $(inputObj).on("mouseleave",function(e){
	        $(this).stop(true).hide();
	    });
		
		// input 에 change 이벤트
		$(inputObj).on("change",function(e){
			var fileExt = this.value.substring(this.value.lastIndexOf("."));
			
			if(!fileExt.match(extRegExp)){
				inputObj.replaceWith(inputObj = inputObj.clone(true));
				alert("업로드 가능한 파일종류가 아닙니다.\n"+ opts.fileType.replace("|",",") +"만 업로드 가능합니다.");
				return;
			}
			imagePreview(e.target, $(divObj));
		});

		// 이미지 미리 로딩
		if (chkHtml5){
			if (opts.defaultImgUrl != ""){
				$(divObj).css({
					"background-image" : "url('"+ opts.defaultImgUrl +"')",
	                "background-size" : "contain",
	                "background-repeat" : "no-repeat",
				}); 
			}	
		
		}else{
			// 위에 css로 올림
            //var newPreview = $(divObj);
            //newPreview[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = opts.defaultImgUrl;
		}
		
		// 동적으로 업로드 처리하기 위한 form 생성.
		if (opts.asyncUpload){
			formObj = $("<form>",{
				id: opts.formName,
				method: "post",
				enctype: "multipart/form-data",
				action: "/lvtmgmt/common/fileUploadOreillyAjaxText.lv?properties="+opts.globalProperties
			});

			if (!chkHtml5){
				$(formObj).ajaxForm({
					dataType: "json",
		            async: false,
					beforeSubmit: function(){
						
						if (chkPreUpload){
							return false;
						}
						
						//console.log("beforeSubmit");
						if (inputObj[0].value == ""){
							if(opts.notUploadAlertMsg)
								alert("저장할 파일이 없습니다.");
							return false;
						}else{
							return true;
						}
						
					},
					success: function(data){
						//console.log("success: "+ data);
						if (opts.returnPath == "Y"){
							$(inputRtnObj).val(data.saveFileUrl);	
						}else{
							$(inputRtnObj).val(data.saveFileName);
						}
						
						if (opts.rtnFunc != null)
							opts.rtnFunc();
					},
					error: function(data){
						//console.log("error:"+ data);
					},
					complete: function(xhr){
						//console.log("complete:"+ xhr.responseText);
					}
				});
				//properties 값  넘김.
				inputPropertiesObj.appendTo(formObj);
				//PrevFileName 값  넘김.
				inputPrevFileNameObj.appendTo(formObj);
				//NextFileName 값  넘김.
				inputNextFileNameObj.appendTo(formObj);
			}else{
				// html5 일때는 기본으로 onsubmit 에 return false 처리.
				$(formObj).on("submit",function(e){
					if(opts.notUploadAlertMsg)
						alert("저장할 파일이 없습니다.");
					return false;
				});
			}

			inputObj.appendTo(formObj);
			inputRtnObj.appendTo(formObj);
			
		}else{
			// form 을 대신  div 생성함..
			formObj = $("<div>",{
			});
			inputObj.appendTo(formObj);
			inputRtnObj.appendTo(formObj);
		}

		return this.html(divObj).after(formObj).before(closeIconObj);
	};
	
	// 기본 세팅값 
	$.fn.dndFileUpload.defaults = {
			globalProperties: "",
			asyncUpload: false,
			notUploadAlertMsg: true,
			formName: "frmFileUpload_"+ parseInt($.now() * Math.random()).toString(),
			fileType: ".jpg|.png|.jpeg", /* | 로 구분 */
			fileSize: "10",  /* 허용할 용량. 숫자값만.. 단위는 Mega */
			imgWidth: "200", /* 숫자값만.. */
			imgHeight: "40", /* 숫자값만.. */
			inputFileName: "iptFileUpload_"+ parseInt($.now() * Math.random()).toString(), /* input name 값 없으면 랜덤하게 */
			inputRtnFileName: "iptRtnFileName_"+ parseInt($.now() * Math.random()).toString(), /* input name 값 없으면 랜덤하게 */
			defaultImgUrl: "", /* 페이지 로딩시 기본으로 표시할 이미지 */
			returnPath: "Y", /* 리턴받는 값에 경로 포함여부 기본값은 Y, 파일명만 받고 싶으면 N*/
			closeIconUse: "Y", /* 닫기 아이콘 사용여부 */
			closeIconUrl: "/plugin/jqGrid/css/images/ui-icons_888888_256x240.png", /* 기본으로 jqgrid 에 있는 아이콘 사용함 ^^;;설마 안쓰진 않겠지.. */
			closeIconX: "-97px",
			closeIconY: "112px",
			closeIconWidth: "15",
			closeIconHeight: "15",
			rtnFunc: null,
			prevFileName: "",
			nextFileName: "",
	};
	
}(jQuery));
