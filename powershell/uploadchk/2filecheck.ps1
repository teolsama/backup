#<BEGIN_SCRIPT>#

#<Set Path to be monitored>#
$searchPath = "D:\_WORK\upload2"

#<Set Watch routine>#
$watcher = New-Object System.IO.FileSystemWatcher
$watcher.Path = $searchPath
$watcher.IncludeSubdirectories = $false
$watcher.EnableRaisingEvents = $true

#<Set Events>#
$changed = Register-ObjectEvent $watcher "Changed" -Action {
   write-host "Changed: $($eventArgs.FullPath)"
   write-host "searchPath: $searchPath"
}
$created = Register-ObjectEvent $watcher "Created" -Action {
   Write-EventLog Application -source Powershell -EventId 54321 -Message "A new fax has been received."
}
#$deleted = Register-ObjectEvent $watcher "Deleted" -Action {
#   write-host "Deleted: $($eventArgs.FullPath)"
#}
#$renamed = Register-ObjectEvent $watcher "Renamed" -Action {
#   write-host "Renamed: $($eventArgs.FullPath)"
#}

#<END_SCRIPT>#