﻿# folder 에 파일이 새로 생생될때 특수문자나 한글이 들어가 있으면 destination 폴더로 이동 시켜 버리는 스크립트
# 텔레그램 알림 추가

Set-Variable -Name folder -Scope Global -Value "D:\_WORK\upload2"
Set-Variable -Name destination -Scope Global -Value "D:\_WORK\upload2move"
Set-Variable -Name extFilter -Scope Global -Value "*.jpg"

#$folder = 'D:\_WORK\upload2\'
#$extFilter = '*.jpg'                             # <-- set this according to your requirements
#$destination = 'D:\_WORK\upload2move\'

$fsw = New-Object IO.FileSystemWatcher $folder, $extFilter -Property @{
	IncludeSubdirectories = $true              # <-- set this according to your requirements
	NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'
}
$onCreated = Register-ObjectEvent $fsw Created -SourceIdentifier FileCreated -Action {
	$path = $Event.SourceEventArgs.FullPath
	$name = $Event.SourceEventArgs.Name
	#Write-Host "path : $path"
	#Write-Host "name : $name"
	
	#한글이나 특수문자 있으면 파일 이동 시킴
	if(($name -match "[가-힣]") -or ($name -match "[\{\}\[\]\(\)\'\-\=\'\#,;*~`!^+@]"))
	{
		# 텔레그램 알림
		$telegramConfig = Get-Content .\config.json -encoding utf8 | ConvertFrom-Json
		$encodedMessage = [System.Web.HttpUtility]::UrlEncode("'"+$name + "'`r`n한글이나 특수문자가 들어가 있으면 변환이 되지 않습니다. 해당 파일은 삭제되었습니다.")
		$info = Invoke-RestMethod -Uri "https://api.telegram.org/bot$($telegramConfig.token)/sendMessage?text=$encodedMessage&chat_id=$($telegramConfig.chatId)"
		#Write-Host "info : $info"
		
		$changeType = $Event.SourceEventArgs.ChangeType
		$timeStamp = $Event.TimeGenerated
		$startNum = 1
		# 옮길 곳에 이미 파일명 있으면 숫자 붙임.
		Get-ChildItem -Path $path -Recurse | ForEach-Object {

			$nextName = Join-Path -Path $destination -ChildPath $_.name

			while(Test-Path -Path $nextName)
			{
				# 파일명 변경
			   $nextName = Join-Path $destination ($_.BaseName + " ($startNum)" + $_.Extension)
			   $startNum += 1   
			}
			$_ | Move-Item -Destination $nextName
		}
	#Move-Item -Path $path -Destination $destination -Force -Verbose # Force will overwrite files with same name
	}
}



# 텔레그램 작업순서
# 참고 url 
# http://www.haruair.com/blog/3664
# http://hahahahatr.blogspot.kr/2016/05/chat-id-how-to-get-private-channel-of.html

# @BotFather 에게 /newBot 으로 말을 건다.
# 봇 이름과 대화명 등을 물어보는데 답변해주면 api 값을 던져준다.
# (예: lfuploadchk , lfuploadchk_bot)
# 잘 메모해놓고
# config.json 에 token 에 세팅해놓는다.
# telegram.ps1 을 실행시켜보면 아까 입력햇던 봇이름, 대화명등을 id,username 값으로 알려준다.
# 이제 텔레그램 앱에서 채널을 생성한다.
# 공개 채널로 하고 링크주소를 만든다 (예 : lfuploadchk)
# 채널이 생성되었으면 채널 설정 화면으로 가서 관리자를 추가한다. 
# 관리자 아이디는 위에 입력했던 봇이름으로 검색해본다.(예: lfuploadchk_bot)
# 관리자 권한에 메세지 작성이 체크되어 있는지 확인하고 생성완료한다.
# 여기까지 하면 봇에 명령을 내려서 방금생성한 채널에 채팅을 보낼 수 있다.
# config.json 에 chatId 값에 아까 위에 생성한 채널주소 @lfuploadchk 를 입력한다.
# 여기서 message.ps1 을 실행시켜 보면 봇이 '테스트'라는 채팅을 날리고 채널에 대한 정보를 볼 수 있다.
# chat       : @{id=-1001103477651; title=LF 파일업로드 체크 봇; username=lfuploadchk; type=channel}
# 위에 id 값이 이 채팅방에 대한 id값인데 이값을 복사해서
# config.json chatId 값에 넣는다.
# 여기서 다시 message.ps1 을 실행시켜보면 '테스트' 라는 채팅이 다시 들어온것을 볼 수 있다.
# 이제 채팅방을 비공개로 하여도 봇이 메세지를 날릴 수 있게된다.
