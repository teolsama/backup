﻿$config = Get-Content .\config.json -encoding utf8 | ConvertFrom-Json
$encodedMessage = [System.Web.HttpUtility]::UrlEncode("테스트")
Write-Host "https://api.telegram.org/bot$($config.token)/sendMessage?text=$encodedMessage&chat_id=$($config.chatId)"
$info = Invoke-RestMethod -Uri "https://api.telegram.org/bot$($config.token)/sendMessage?text=$encodedMessage&chat_id=$($config.chatId)"
if ($info.ok) {
	echo $info.result | Format-List
}